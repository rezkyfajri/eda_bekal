﻿using FluentValidation.Results;

namespace Framework.Validation
{
    public static class ValidationError
    {
        public static IError[] Create(ValidationResult result)
        {
            IError[] errors = new IError[result.Errors.Count];
            int index = 0;
            foreach (var item in result.Errors)
            {
                var error = ErrorBuilder.New()
                .SetMessage(item.ErrorMessage)
                .SetCode(item.PropertyName)
                .Build();
                errors[index++] = error;
            }
            return errors;
        }

    }
}

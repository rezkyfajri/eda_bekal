﻿using FluentValidation;
using LookUp.Domain.Dtos;
using LookUp.Domain.Entities;
namespace LookUp.Domain.Validations
{
    public class AttributeAddValidator : AbstractValidator<AttributeDto>
    {
        public AttributeAddValidator(LookUpDbContext dbContext) 
        {
            RuleFor(x => x.Unit)
                    .NotNull()
                    .WithName("Unit")
                    .WithMessage("Unit Is Required");

            RuleFor(x => x.Unit)    
              .Length(1, 100)
              .WithName("Unit")
              .WithMessage("Length 1 - 100");

            RuleFor(x => x.Type)
                .NotNull()
                .WithName("Type")
                .WithMessage("Type Is Required");

            RuleFor(x => new { x.Unit })
                .Must(x =>
                {
                    return !dbContext.Attributes.Where(o => o.Unit == x.Unit).Any();
                })
                .WithName("Unit")
                .WithMessage("Unit Already Exist");


        }
    }
}

﻿using FluentValidation;
using FluentValidation.Results;
using Framework.Validation;
using LookUp.Domain.Dtos;
using LookUp.Domain.Services;

namespace LookUp.GraphQL.Schema.Mutation
{
    [ExtendObjectType(typeof(Mutation))]
    public class AttributeMutation
    {
        private readonly IAttributeService _service;
        private IValidator<AttributeExceptStatusDto> _validatorUpdate;
        private IValidator<AttributeDto> _validatorAdd;
        public AttributeMutation(IAttributeService service, IValidator<AttributeExceptStatusDto> validatorUpdate, IValidator<AttributeDto> validatorAdd)
        {
            _service = service;
            _validatorUpdate = validatorUpdate;
            _validatorAdd = validatorAdd;
        }

        public async Task<AttributeDto> AddAttributeAsync(AttributeDto dto)
        {
            ValidationResult resultVal = await _validatorAdd.ValidateAsync(dto);
            if(!resultVal.IsValid)
            {
                throw new GraphQLException(ValidationError.Create(resultVal));

            }
            var result = await _service.AddAttribute(dto);
            return result;
        }
        public async Task<AttributeExceptStatusDto> Update(AttributeExceptStatusDto dto)
        {
            ValidationResult resultVal = await _validatorUpdate.ValidateAsync(dto);
            if (!resultVal.IsValid)
            {
                throw new GraphQLException(ValidationError.Create(resultVal));
            }
            else
            {
                var result = await _service.UpdateAttributes(dto);
                if (result) return dto;
            }
            return null;

        }
        public async Task<AttributeStatusDto> ChangeStatus(AttributeStatusDto dto)
        {
            try
            {
                var result = await _service.ChangeStatus(dto);
                if (result) return dto;
            }
            catch (Exception e)
            {
                await Console.Out.WriteLineAsync($"Error: {e.Message}");
                throw;
            }
            return null;
        }
    }
}

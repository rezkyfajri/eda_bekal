﻿namespace Store.GraphQL.Schema.inputs
{
    public class ProductCreateInput
    {
        public Guid CategoryId { get; set; }
        public Guid AttributeId { get; set; }
        public string Sku { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        
    }
}
